# Linki do artykułów

Każda pozycja zawiera link do lokalnego dokumentu PDF. Artykuły otwarto źródłowe zawierają także URL do zasobu w Internecie.

## General

- Surveillance in Action - Chapter 16, Methods to Detect Cyberthreats on Twitter, [PDF](pobrane_dokumenty/2018_Book_SurveillanceInAction_Chapter_16.pdf)

- A Survey on Machine Learning Techniques for Cyber Security in the Last Decade, [PDF](pobrane_dokumenty/A_Survey_on_Machine_Learning_Techniques_for_Cyber_Security_in_the_Last_Decade.pdf), <https://ieeexplore.ieee.org/ielx7/6287639/6514899/09277523.pdf>

- A Cyber Kill Chain Approach for Detecting Advanced Persistent Threats, [PDF](pobrane_dokumenty/A_Cyber_Kill_Chain_Approach_for_Detecting_APTs.pdf), <https://www.techscience.com/cmc/v67n2/41316/html>

- A new proposal on the advanced persistent threat: A survey, [PDF](pobrane_dokumenty/A_New_Proposal_on_the_APT_A_Survey.pdf), <https://www.mdpi.com/2076-3417/10/11/3874/htm>

## Phishing / Malicious URLs

- Machine Learning based Phishing Website
Detection System, [PDF](pobrane_dokumenty/machine-learning-based-phishing-website-detection-system-.pdf), <https://www.ijert.org/research/machine-learning-based-phishing-website-detection-system-IJERTV4IS120262.pdf>

- A malicious URLs detection system using optimization and machine learning classifiers, [PDF](pobrane_dokumenty/A_malicious_URLs_detection_system_using_optimization_and_machine_learning_classifiers.pdf), <http://ijeecs.iaescore.com/index.php/IJEECS/article/view/21154>

## Network

- Machine learning and deep learning methods for intrusion detection systems: A survey, [PDF](pobrane_dokumenty/ML_and_DL_for_IDS.pdf)

- IntruDTree: A Machine Learning Based Cyber Security Intrusion Detection Model, [PDF](pobrane_dokumenty/IntruDTree.pdf), <https://www.mdpi.com/2073-8994/12/5/754/htm>

- Network intrusion detection system: A systematic study ofmachine learning and deep learning approaches, [PDF](pobrane_dokumenty/Network_intrusion_detection_system_A_systematic_study_ofmachine_learning_and_deep_learning_approaches.pdf), <https://onlinelibrary.wiley.com/doi/full/10.1002/ett.4150>

- Performance Analysis of Machine Learning Algorithms in Intrusion Detection System: A Review, [PDF](pobrane_dokumenty/Performance_Analysis_of_Machine_Learning_Algorithms_in_Intrusion_Detection_System_A_Review.pdf), <https://www.sciencedirect.com/science/article/pii/S1877050920311121?pes=vor>

## IoT

- Classification of botnet attacks in IoT smart factory using honeypot combined with machine learning, [PDF](pobrane_dokumenty/Classification_of_botnet_attacks_in_IoT_smart_factory_using_honeypot_combined_with_ML.pdf)

- Denial of service attack detection through machine learning for the IoT, [PDF](pobrane_dokumenty/Denial_of_service_attack_detection_through_machine_learning_for_the_IoT.pdf), <https://www.tandfonline.com/doi/full/10.1080/24751839.2020.1767484>

## Malicious images

- MalJPEG: Machine Learning Based Solution for
the Detection of Malicious JPEG Images, [PDF](pobrane_dokumenty/MalJPEG_Machine_Learning_Based_Solution_for_the_Detection_of_Malicious_JPEG_Images.pdf), <https://ieeexplore.ieee.org/ielx7/6287639/8948470/08967109.pdf>
